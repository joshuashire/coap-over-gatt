# CoAP over GATT (Bluetooth Low Energy Generic Attributes)

This is the working area for the individual Internet-Draft, "CoAP over GATT (Bluetooth Low Energy Generic Attributes)".

* [Editor's Copy](https://chrysn.gitlab.io/coap-over-gatt/)
* [Individual Draft](https://tools.ietf.org/html/draft-amsuess-core-coap-over-gatt)

## Building the Draft

Formatted text and HTML versions of the draft can be built using `make`.

```sh
$ make
```

This requires that you have the necessary software installed.  See
[the instructions](https://github.com/martinthomson/i-d-template/blob/master/doc/SETUP.md).


## Contributing

See the
[guidelines for contributions](https://gitlab.com/chrysn/coap-over-gatt/-/blob/master/CONTRIBUTING.md).
