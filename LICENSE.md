# License

See the
[guidelines for contributions](https://github.com/git@gitlab.com:chrysn/coap-over-gatt/blob/master/CONTRIBUTING.md).
